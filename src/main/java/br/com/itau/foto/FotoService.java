package br.com.itau.foto;

import java.security.Principal;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FotoService {

	@Autowired
	FotoRepository fotoRepository;
	
	public Foto inserir(Foto foto, Principal principal) {
		foto.setTimestamp(LocalDateTime.now());
		foto.setNomeUsuario(principal.getName());
		
		return fotoRepository.save(foto);
	}
	
	public Iterable<Foto> buscarTodas(){
		return fotoRepository.findAll();
	}

}
